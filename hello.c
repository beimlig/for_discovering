#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

#define CLEAR system("clear")

int main (){
	
	char* original = "hello world";
	int original_size = 0;
	char* ptr = original;

	//just counting original_size
	while (*ptr++ != '\0')
	{
		++original_size;
	}

	//make space for the new string on the heap 
	char* result = calloc(original_size, 1);

	//call srand() for random numbers since wi did not start from 'a'
	srand(time(NULL));
	char random_char;
	int random_number_for_string;
	for (int i = 0; i < original_size;)
	{
		if(original[i] >= 32 && original[i] <= 47)
		{
			random_number_for_string = rand () % 15; //for letter which is not 
			random_char = random_number_for_string + 32;
		}
		else
		{
			random_number_for_string = rand () % 25; //we modulus 25 because we have 25 letter int english  abc
			random_char = random_number_for_string + 97; //we add 97 for ascii
		}
		result[i] = random_char;
		
		printf("%s\n", result);

		usleep(45000);
		//CLEAR; //if you uncomment this you will get better looking terminal 

		if (result[i] == original[i])
		{
			i++;
		}
			
	}
	//printf("%s", result);		//uncomment this  
	//getchar();				//and this 2 lines only if you uncomment clear 

	return 0;
}
